package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"text/template"
	"time"
)

const telegramAPIBaseURL string = "https://api.telegram.org/bot"
const telegramAPISendMessage string = "sendMessage"

var (
	telegramChatID   = os.Getenv("TELEGRAM_CHAT_ID")
	telegramBotToken = os.Getenv("TELEGRAM_BOT_TOKEN")
)

var telegramAPI string = fmt.Sprintf("%s%s/%s", telegramAPIBaseURL, telegramBotToken, telegramAPISendMessage)

type dataProm struct {
	Version     string `json:"version"`
	GroupKey    string `json:"groupKey"`
	Status      string `json:"status"`
	Receiver    string `json:"receiver"`
	GroupLabels struct {
		Alertname string `json:"alertname"`
	} `json:"groupLabels"`
	CommonLabels struct {
		Alertname string `json:"alertname"`
		Monitor   string `json:"monitor"`
		Severity  string `json:"severity"`
	} `json:"commonLabels"`
	CommonAnnotations struct {
		Summary string `json:"summary"`
	} `json:"commonAnnotations"`
	ExternalURL string `json:"externalURL"`
	Alerts      []struct {
		Labels struct {
			Alertname string `json:"alertname"`
			Instance  string `json:"instance"`
			Job       string `json:"job"`
			Monitor   string `json:"monitor"`
			Severity  string `json:"severity"`
		} `json:"labels"`
		Annotations struct {
			Description string `json:"description"`
			Summary     string `json:"summary"`
		} `json:"annotations"`
		StartsAt string    `json:"startsAt"`
		EndsAt   time.Time `json:"endsAt"`
	} `json:"alerts"`
}

// Handler is called everytime Telegram sends us a webhook event
func Handler(res http.ResponseWriter, req *http.Request) {
	// First, decode the JSON response body
	body := &dataProm{}
	if err := json.NewDecoder(req.Body).Decode(body); err != nil {
		fmt.Println("Could not decode request body", err)
		return
	}

	//
	if err := sendMessageGroup(body); err != nil {
		fmt.Println("Error in sending reply:", err)
		return
	}

	// Log a confirmation message if the message is sent successfully
	fmt.Println("Reply sent")
}

// The below code deals with the process of sending a response message
// to the group/user

// Create a struct to confirm to the JSON body
// of the send messafe request
type sendMessageReqBody struct {
	ChatID int64  `json:"chat_id"`
	Text   string `json:"text"`
}

// sendMessageGroup takes a chatID and sends to them
func sendMessageGroup(dataProm *dataProm) error {
	// Create the request body struct
	data := loadTemplate(dataProm)
	chatID, _ := strconv.ParseInt(telegramChatID, 10, 64)
	reqBody := &sendMessageReqBody{
		ChatID: chatID,
		Text:   string(data),
	}
	// Create the JSON body from the struct
	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	// Send a post request with your token
	res, err := http.Post(telegramAPI, "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("Unexpected status" + res.Status)
	}
	return nil
}

// loadTemplate takes a template and parse, then send string to buffer
func loadTemplate(data *dataProm) string {
	message, err := ioutil.ReadFile("default.tmpl")
	if err != nil {
		return ""
	}

	t, err := template.New("Parse").Parse(string(message))
	if err != nil {
		return ""
	}

	var buffer bytes.Buffer
	err = t.Execute(&buffer, data)
	if err != nil {
		return ""
	}

	return buffer.String()
}

func main() {
	http.ListenAndServe(":3000", http.HandlerFunc(Handler))
}
